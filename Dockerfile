FROM alpine:3.17
RUN apk add nginx nginx-mod-http-fancyindex dumb-init
ADD vhosts.conf /etc/nginx/http.d
ADD 10_http_fancyindex.conf /etc/nginx/modules
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["nginx", "-g", "daemon off;"]
EXPOSE 80
